[setroubleshoot](https://gitlab.com/setroubleshoot/setroubleshoot) re-implemented in Rust

# usage

1. `dnf install setroubleshoot-server`

1. `cargo build`

1. `sudo cp target/debug/setroubleshootd /sbin`

1. `sudo cp python/plugin-wrapper.py /usr/share/setroubleshoot/`

1. `cat db.sql | sudo sqlite3 /var/lib/setroubleshoot/setroubleshootd.db &&
    sudo chown setroubleshoot:setroubleshoot /var/lib/setroubleshoot/setroubleshootd.db`

1. `sudo dbus-send --system --print-reply --type=method_call --dest=org.fedoraproject.Setroubleshootd /org/fedoraproject/Setroubleshootd org.fedoraproject.SetroubleshootdIface.avc string:'type=AVC msg=audit(1671208212.678:3651): avc:  denied  { read } for  pid=908557 comm="gdb" name="pcmC2D0p" dev="devtmpfs" ino=3684 scontext=system_u:system_r:abrt_t:s0-s0:c0.c1023 tcontext=system_u:object_r:sound_device_t:s0 tclass=chr_file permissive=1'`
