#!/usr/bin/python3

import json
import sys

import selinux.audit2why as audit2why


# audit2why.init()

from setroubleshoot.audit_data import *
from setroubleshoot.avc_audit import AuditRecordReceiver
from setroubleshoot.util import load_plugins
from setroubleshoot.config import config_init

size = int(sys.stdin.readline())

data = sys.stdin.read(size)
while len(data) < size:
    data += sys.stdin.read(size - len(data))

record_reader = AuditRecordReader(AuditRecordReader.TEXT_FORMAT)
record_receiver = AuditRecordReceiver()

avcs = []
while True:
    try:
        audit2why.init()
    except:
        continue
    else:
        break

for (record_type, event_id, body_text, fields, line_number) in record_reader.feed(data):
    audit_record = AuditRecord(record_type, event_id, body_text, fields, line_number)
    audit_record.audispd_rectify()
    # print("%s, %s, %s, %s, %s" % (record_type, event_id, body_text, fields, line_number))
    # print(f"audit_record {audit_record}")


    for audit_event in record_receiver.feed(audit_record):
        # self.add_audit_event(audit_event)
        # print(f"inside {audit_event}")
        avcs.extend(compute_avcs(audit_event))
        # print(f"{avcs}")


for audit_event in record_receiver.flush(0):
    # self.add_audit_event(audit_event)
    # print(f"outside {audit_event} outside")
    avcs.extend(compute_avcs(audit_event))
    # print(f"avcs {avcs}")
audit2why.finish()

config_init()
plugins = load_plugins()

analyze_report = {}

def get_report_texts(plugin, report):
    texts = {}
    texts['args'] = report.args
    texts['if_text'] = plugin.get_if_text(avc, report.args)
    texts['then_text'] = plugin.get_then_text(avc, report.args)
    texts['do_text'] = plugin.get_do_text(avc, report.args)
    # texts['fix_cmd'] = plugin.get_fix_cmd(avc, report.args)
    texts['problem_desc'] = plugin.get_problem_description(avc, report.args)

    return texts

for avc in avcs:
    for plugin in plugins:
        report = plugin.analyze(avc)
        if report is None:
            continue
        if isinstance(report, list):
            for r in report:
                analyze_report[r.analysis_id] = get_report_texts(plugin, r)
        else:
            analyze_report[report.analysis_id] = get_report_texts(plugin, report)

print(json.dumps(analyze_report))
