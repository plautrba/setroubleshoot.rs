use super::event::Event;

///        'host': {'XMLForm': 'element', },
///        'access': {'XMLForm': 'element', 'list': 'operation', },
///        'scontext': {'XMLForm': 'element', 'import_typecast': AvcContext},
///        'tcontext': {'XMLForm': 'element', 'import_typecast': AvcContext},
///        'tclass': {'XMLForm': 'element', },
///        'port': {'XMLForm': 'element', 'import_typecast': int, },

#[derive(Debug)]
pub struct AVC {
    pub scontext: String,
    pub tcontext: String,
    pub tclass: String,
    pub seperms: String,
}

impl AVC {
    pub fn new(event: &Event) -> AVC {
        let mut signature = AVC {
            scontext: String::new(),
            tcontext: String::new(),
            tclass: String::new(),
            seperms: String::new(),
        };

        let mut seperms: Vec<String> = Vec::new();

        for avc in event.avcs.iter() {
            if let Some(scontext) = avc.get("scontext") {
                signature.scontext = scontext.clone();
            }
            if let Some(tcontext) = avc.get("tcontext") {
                signature.tcontext = tcontext.clone();
            }
            if let Some(tclass) = avc.get("tclass") {
                signature.tclass = tclass.clone();
            }
            if let Some(a_seperms) = avc.get("seperms") {
                seperms.push(a_seperms.to_string());
            }
        }
        seperms.sort();
        signature.seperms = seperms.join(",");
        signature
    }
}

mod tests {
    use super::*;

    #[test]
    fn test_signature_from_data() {
        use std::fs;
        let data: String = fs::read_to_string("audit.log").unwrap();
        let event = Event::from(&data).unwrap();
        // println!("{:#?}", event);
        let signature = AVC::new(&event);
        println!("{:?}", signature);
    }

    #[test]
    fn test_empty_string() {
        let s1 = String::new();
        let s2 = String::from("");
        assert!(s1 == s2);
    }
}
