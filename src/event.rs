#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
include!("./auparse-bindings.rs");

use std::collections::HashMap;

use std::ffi::{c_void, CStr};

use std::fmt;

type AuditRecord = HashMap<String, String>;

use serde::{Deserialize, Serialize};
use serde_json::Result;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Event {
    pub rowid: Option<i64>,
    pub data: String,
    pub avcs: Vec<AuditRecord>,
    pub others: Vec<AuditRecord>,
}

impl fmt::Display for Event {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        let j = serde_json::to_string(self).unwrap();
        write!(f, "{}", j)
    }
}

impl Event {
    pub fn from(data: &String) -> Option<Event> {
        // auparse needs records ending with a new line and \0
        let buffer: String = String::from(data) + "\n\0";
        let buffer_ptr: *const c_void = buffer.as_str().as_ptr() as *const c_void;
        let mut au_type;
        let au;
        let mut r;
        let mut event = Event {
            rowid: None,
            data: data.clone(),
            avcs: Vec::new(),
            others: Vec::new(),
        };
        unsafe {
            au = auparse_init(ausource_t_AUSOURCE_BUFFER, buffer_ptr);
            r = auparse_first_record(au);
            while r > 0 {
                let au_type_ptr = auparse_get_type_name(au);
                if au_type_ptr.is_null() {
                    return None;
                }
                au_type = CStr::from_ptr(au_type_ptr as *const _).to_str().unwrap();
                let mut record = AuditRecord::new();
                while r > 0 {
                    let field_name_ptr = auparse_get_field_name(au);
                    let field_name = CStr::from_ptr(field_name_ptr as *const _).to_str().unwrap();
                    // let field_str_ptr = auparse_get_field_str(au);
                    let field_str_ptr = auparse_interpret_field(au);
                    let field_str = CStr::from_ptr(field_str_ptr as *const _).to_str().unwrap();
                    // println!("field_name: {}={}", field_name, field_str);
                    record.insert(String::from(field_name), String::from(field_str));

                    r = auparse_next_field(au);
                }
                match au_type {
                    "AVC" | "USER_AVC" | "1400" | "1107" => event.avcs.push(record),
                    _ => event.others.push(record),
                }
                // println!("type: {}", au_type);
                r = auparse_next_record(au);
                // println!("r: {}", r);
            }
        };
        Some(event)
    }
    pub fn from_json(data: &str) -> Option<Event> {
        // println!("from_json {}", data);
        let event: Result<Event> = serde_json::from_str(data);
        match event {
            Ok(e) => return Some(e),
            Err(e) => println!("{:?}", e),
        }
        None
    }

    pub fn get_field(&self, au_type: Option<&str>, field: &str) -> Option<String> {
        if let Some(au_type) = au_type {
            if au_type == "AVC" || au_type == "USER_AVC" {
                for record in self.avcs.iter() {
                    if let Some(record_field) = record.get(&field.to_string()) {
                        return Some(record_field.clone());
                    }
                }
            } else {
                for record in self.others.iter() {
                    if let Some(record_type) = record.get("type") {
                        if record_type == au_type {
                            return record.get(&field.to_string()).cloned();
                        }
                    }
                }
            }
        } else {
            for record in self.others.iter() {
                if let Some(record_field) = record.get(&field.to_string()) {
                    return Some(record_field.clone());
                }
            }
            for record in self.avcs.iter() {
                if let Some(record_field) = record.get(&field.to_string()) {
                    return Some(record_field.clone());
                }
            }
        }
        None
    }
}

mod tests {
    use super::*;

    #[test]
    fn test_event_from_data() {
        use std::fs;
        let data: String = fs::read_to_string("audit.log").unwrap();
        let event = Event::from(&data);
        println!("{:#?}", event);
    }

    #[test]
    fn test_empty_string() {
        let s1 = String::new();
        let s2 = String::from("");
        assert_eq!(s1, s2);
    }

    #[test]
    fn test_event_get_field() {
        use std::fs;
        let data: String = fs::read_to_string("audit.log").unwrap();
        let event = Event::from(&data).unwrap();
        let mut field = event.get_field(Some("SYSCALL"), "exe");
        println!("{:?}", field);
        assert_eq!(Some("/usr/bin/cat".to_string()), field);
        field = event.get_field(Some("PROCTITLE"), "proctitle");
        assert_eq!(Some("cat /etc/passwd".to_string()), field);

        field = event.get_field(None, "SYSCALL");
        assert_eq!(Some("execve".to_string()), field);
    }
}
