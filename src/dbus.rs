use crate::event::Event;

use dbus::blocking::Connection;
use dbus_crossroads::{Context, Crossroads, MethodErr};
use std::error::Error;

use std::sync::mpsc::{Receiver, Sender, TryRecvError};

use std::thread;

#[derive(Debug)]
struct State {
    running: bool,
    counter: u32,
    tx: Sender<Option<Event>>,
}

fn hello(
    ctx: &mut Context,
    state: &mut State,
    (name,): (String,),
) -> Result<(String,), dbus_crossroads::MethodErr> {
    // And here's what happens when the method is called.
    println!("Incoming hello call from {}!", name);
    let s = format!("Hello {}! I received {} AVCs.", name, state.counter);
    // The ctx parameter can be used to conveniently send extra messages.
    let signal_msg = ctx.make_signal("HelloHappened", (name,));
    ctx.push_msg(signal_msg);
    // And the return value is a tuple of the output arguments.
    Ok((s,))
}

fn start(
    _: &mut Context,
    state: &mut State,
    _: (),
) -> Result<(&'static str,), dbus_crossroads::MethodErr> {
    println!("Start: {:?}", state);
    // And the return value is a tuple of the output arguments.
    state.running = true;
    Ok(("Started",))
}

fn finish(
    _: &mut Context,
    state: &mut State,
    _: (),
) -> Result<(&'static str,), dbus_crossroads::MethodErr> {
    // And the return value is a tuple of the output arguments.
    state.running = false;
    println!("Finish: {:?}", state);
    Ok(("Finished",))
}

fn avc(
    _: &mut Context,
    state: &mut State,
    (data,): (String,),
) -> Result<(&'static str,), dbus_crossroads::MethodErr> {
    //let buffer: String = data + "\0";
    let event = Event::from(&data);
    if event.is_none() {
        return Err(MethodErr::invalid_arg("no avc"));
    }
    let mut event_data = event.unwrap();
    if event_data.avcs.is_empty() {
        return Err(MethodErr::invalid_arg("no avc"));
    }
    event_data.rowid = match crate::db::add_avc(&event_data.to_string()) {
        Ok(rowid) => Some(rowid),
        Err(err) => {
            println!("error add_avc: {}", err);
            None
        }
    };
    state.counter += 1;
    state.tx.send(Some(event_data)).unwrap();
    Ok(("AVC",))
}

pub fn run(
    tx: Sender<Option<Event>>,
    analyzer_rx: Receiver<Option<Event>>,
) -> Result<(), Box<dyn Error>> {
    println!("ahoj");
    let c = Connection::new_system()?;
    // https://dbus.freedesktop.org/doc/api/html/group__DBusShared.html#ga40b7a263caf8d6e39b39a8c7350a084f
    // https://dbus.freedesktop.org/doc/api/html/group__DBusShared.html#ga0f16ed23be47eb22a37a227a6e39597b
    // https://dbus.freedesktop.org/doc/api/html/group__DBusShared.html#ga3c18c35e4c2dfbe9a0a5be47f4b4b6e9
    c.request_name("org.fedoraproject.Setroubleshootd", false, true, false)?;

    // Create a new crossroads instance.
    // The instance is configured so that introspection and properties interfaces
    // are added by default on object path additions.
    let mut cr = Crossroads::new();

    // Let's build a new interface, which can be used for "Hello" objects.
    let iface_token = cr.register("org.fedoraproject.SetroubleshootdIface", |b| {
        // This row is just for introspection: It advertises that we can send a
        // HelloHappened signal. We use the single-tuple to say that we have one single argument,
        // named "sender" of type "String".
        b.signal::<(String, String), _>("alert", ("level", "local_id"));
        // Let's add a method to the interface. We have the method name, followed by
        // names of input and output arguments (used for introspection). The closure then controls
        // the types of these arguments. The last argument to the closure is a tuple of the input arguments.
        b.method("start", (), ("reply",), start);
        b.method("finish", (), ("reply",), finish);
        b.method("avc", ("data",), ("reply",), avc);
        b.method("Hello", ("name",), ("reply",), hello);
    });

    let state_tx = tx.clone();
    cr.insert(
        "/org/fedoraproject/Setroubleshootd",
        &[iface_token],
        State {
            running: false,
            counter: 0,
            tx: state_tx,
        },
    );

    // https://docs.rs/dbus-crossroads/latest/src/dbus_crossroads/crossroads.rs.html#317-327
    use dbus::channel::MatchingReceiver;
    c.start_receive(
        dbus::message::MatchRule::new_method_call(),
        Box::new(move |msg, conn| {
            cr.handle_message(msg, conn).unwrap();

            true
        }),
    );

    // Serve clients forever.
    let mut finished = 0;
    loop {
        let r = c.process(std::time::Duration::from_secs(10))?;
        println!("tik {:?}", r);
        loop {
            match analyzer_rx.try_recv() {
                Err(TryRecvError::Empty) => {
                    break;
                }
                Err(TryRecvError::Disconnected) => {
                    finished = 1;
                    break;
                }
                Ok(e) => {
                    match e {
                        Some(e) => {
                            println!("ANALYZED: {:?}", e.rowid);
                            finished = 0;
                            continue;
                        }
                        None => finished = 1,
                    }
                    // println!("{:?}", e.rowid);
                    break;
                }
            }
        }
        if finished == 1 {
            break;
        }
        if !r {
            tx.send(None).unwrap();
        }
    }
    // cr.serve(&c)?;
    //    c.release_name();
    println!("0 Quit.");
    drop(c);
    Ok(())
}
