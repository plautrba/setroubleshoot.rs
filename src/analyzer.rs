use crate::event::Event;
use crate::signature::AVC;

use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::sync::mpsc::{channel, Receiver, RecvError, Sender, TryRecvError};
use std::thread;

use std::io::Write;
use std::process::{Command, Stdio};

use serde_json::{Result, Value};

type AnalyzerData = HashMap<String, String>;

struct AnalyzerPool {
    max_threads: usize,
    rx: Receiver<thread::ThreadId>,
    tx: Sender<thread::ThreadId>,
    threads: HashSet<thread::ThreadId>,
}

struct AnalyzerThread {
    tx: Sender<thread::ThreadId>,
}

// https://doc.rust-lang.org/stable/std/thread/fn.panicking.html
impl Drop for AnalyzerThread {
    fn drop(&mut self) {
        println!("thread id stopped: {:?}", std::thread::current().id());
        self.tx.send(thread::current().id()).unwrap();
    }
}

impl AnalyzerPool {
    fn new(size: usize) -> AnalyzerPool {
        let (tx, rx) = channel::<thread::ThreadId>();
        AnalyzerPool {
            max_threads: size,
            rx,
            tx,
            threads: HashSet::new(),
        }
    }

    // FIXME: use Drop and thread::panicking()
    // https://doc.rust-lang.org/stable/std/thread/fn.panicking.html
    pub fn spawn(&mut self, event: Event) {
        while self.threads.len() >= self.max_threads {
            match self.rx.recv() {
                Err(_) => continue,
                Ok(thread_id) => self.threads.remove(&thread_id),
            };
        }

        let t_tx = self.tx.clone();
        let thread_handle = std::thread::spawn(move || {
            println!("thread id: {:?}", std::thread::current().id());
            let _thread = AnalyzerThread { tx: t_tx };
            analyze_event(&event);
        });
        self.threads.insert(thread_handle.thread().id());
    }

    pub fn is_empty(&self) -> bool {
        println!("is_empty {:?}", self.threads);
        self.threads.len() == 0
    }

    pub fn clean_threads(&mut self) {
        while let Ok(thread_id) = self.rx.try_recv() {
            self.threads.remove(&thread_id);
        }
    }
}

fn get_analyzer_data(event: &Event) -> AnalyzerData {
    let mut data = AnalyzerData::new();
    let avc = AVC::new(event);

    data.insert("scontext".to_string(), avc.scontext.clone());
    data.insert("tcontext".to_string(), avc.tcontext.clone());
    data.insert("tclass".to_string(), avc.tclass.clone());
    data.insert("seperms".to_string(), avc.seperms.clone());

    if let Some(tpath) = event.get_field(Some("PATH"), "name") {
        data.insert("tpath".to_string(), tpath);
    } else if let Some(tpath) = event.get_field(Some("AVC"), "path") {
        data.insert("tpath".to_string(), tpath);
    }
    if let Some(spath) = event.get_field(None, "exe") {
        data.insert("spath".to_string(), spath);
    }

    data
}

pub fn analyze_event(event: &Event) {
    println!(
        "{:?} Got: {:?}\nANALYZING",
        std::thread::current().id(),
        event.rowid
    );

    let mut child = Command::new("/usr/share/setroubleshoot/plugin-wrapper.py")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("failed to execute child");

    let mut stdin = child.stdin.take().expect("failed to get stdin");
    write!(stdin, "{}\n{}", event.data.len(), event.data).expect("cannot write to child");

    let output = child.wait_with_output().expect("Failed to read stdout");
    // println!("{:?}", std::str::from_utf8(&output.stdout));
    let v: Value = serde_json::from_str(std::str::from_utf8(&output.stdout).unwrap()).unwrap();
    // let event: Result<Event> = serde_json::from_str(data);

    println!("v={:?}\n****", v);
    for plugin in v.as_object().unwrap().keys() {
        println!("If {} then {} do {}\n***", v[plugin]["if_text"], v[plugin]["then_text"], v[plugin]["do_text"]);
    }


    // event_signature.(scontext,tcontext,...)
    // let event_signature = AVC::new(event);

    //std::thread::sleep(std::time::Duration::from_secs(10));
    println!("{:?} ANALYZING FINISHED", std::thread::current().id());
}

pub fn run(
    tx: Sender<Option<Event>>,
    dbus_rx: Receiver<Option<Event>>,
) -> Result<()> {
    let threads_num = thread::available_parallelism().unwrap().get();
    let mut analyzer = AnalyzerPool::new(threads_num);
    while let Ok(event) = dbus_rx.recv() {
        if let Some(e) = event {
            analyzer.spawn(e.clone());
            //                    analyze_event(&e);
            tx.send(Some(e)).unwrap();
        } else {
            // dbus is done, if we are done, then send None
            analyzer.clean_threads();
            if analyzer.is_empty() {
                tx.send(None).unwrap();
                break;
            } else {
            }
        }
    }
    drop(tx);
    Ok(())
}

mod tests {
    use super::*;

    #[test]
    fn test_get_analyzer_data() {
        use std::fs;
        let data: String = fs::read_to_string("audit.log").unwrap();
        let event = Event::from(&data).unwrap();

        let analyzer_data = get_analyzer_data(&event);
        assert_eq!(
            "/lib64/ld-linux-x86-64.so.2",
            analyzer_data.get("tpath").unwrap()
        );
        assert_eq!("/usr/bin/cat", analyzer_data.get("spath").unwrap());
        assert_eq!(
            "system_u:system_r:timedatex_t:s0",
            analyzer_data.get("scontext").unwrap()
        );
    }

    #[test]
    fn test_analyze_event() {
        use std::fs;
        let data: String = fs::read_to_string("audit.log").unwrap();
        let event = Event::from(&data).unwrap();

        analyze_event(&event);
    }
}
