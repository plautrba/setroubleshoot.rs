use rusqlite::{Connection, Result};

pub fn add_avc(data: &String) -> Result<i64, String> {
    let connection = match Connection::open("/var/lib/setroubleshoot/setroubleshootd.db") {
        Ok(connection) => connection,
        Err(e) => {
            println!("{:?}", e);
            return Err("nope".to_string());
        }
    };

    match connection.execute("INSERT INTO avcs VALUES (?1, ?2)", [data, "1"]) {
        Ok(_) => Ok(connection.last_insert_rowid()),
        Err(e) => {
            println!("{:?}", e);
            Err("nope".to_string())
        }
    }
}
