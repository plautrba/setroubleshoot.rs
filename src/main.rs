use std::error::Error;

pub mod analyzer;
pub mod db;
pub mod dbus;
pub mod event;
pub mod signature;

use crate::event::Event;

use std::sync::mpsc;
use std::thread;

fn main() -> Result<(), Box<dyn Error>> {
    let (dbus_tx, dbus_rx) = mpsc::channel::<Option<Event>>();
    let (analyzer_tx, analyzer_rx) = mpsc::channel::<Option<Event>>();
    let dbus_handle = thread::spawn(move || {
        dbus::run(dbus_tx, analyzer_rx).unwrap();
    });
    let analyze_handle = thread::spawn(move || {
        analyzer::run(analyzer_tx, dbus_rx).unwrap();
    });

    dbus_handle.join().unwrap();
    analyze_handle.join().unwrap();
    Ok(())
}
