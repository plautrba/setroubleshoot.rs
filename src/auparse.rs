#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
include!("./auparse-bindings.rs");

use std::ffi::{c_void, CStr};

pub mod event;
use crate::event::Event;

fn main() {
    //let data: String = fs::read_to_string("audit.log").unwrap() + "\0";
    let data: String = "type=AVC msg=audit(1671208212.678:3651): avc:  denied  { read } for  pid=908557 comm=\"gdb\" name=\"pcmC2D0p\" dev=\"devtmpfs\" ino=3684 scontext=system_u:system_r:abrt_t:s0-s0:c0.c1023 tcontext=system_u:object_r:sound_device_t:s0 tclass=chr_file permissive=1\n".to_string();
    // println!("data: {}", &data);
    let event = Event::from(&data);
    let event_str = event.unwrap();
    let new_event = Event::from_json(&event_str.to_string());

    println!("NEW EVENT: {:?} NEW EVENT", new_event);

    let buffer_ptr: *const c_void = data.as_str().as_ptr() as *const c_void;

    unsafe {
        let buffer = CStr::from_ptr(buffer_ptr as *const _).to_str();
        println!("buffer: {}", buffer.unwrap());
    };

    // let f = "audit.log\0".as_ptr();
    //    let f_ptr: *mut c_void = f as *mut c_void;
    let mut au_type;
    let au;
    let mut r;
    unsafe {
        au = auparse_init(ausource_t_AUSOURCE_BUFFER, buffer_ptr);
        //        let r = auparse_new_buffer(au, buffer_ptr as *const c_char, data_len);
        //      println!("au: {:?}, r: {}", au, r);
        // let r = auparse_next_event(au);
        // println!("r: {}", r);
        // println!("----");
        r = auparse_first_record(au);
        while r > 0 {
            println!("record");
            //    let r = auparse_next_record(au);
            //        println!("r: {}", r);

            let au_type_ptr = auparse_get_type_name(au);
            println!("----");
            println!("{:?}", au_type_ptr);
            au_type = CStr::from_ptr(au_type_ptr as *const _).to_str().unwrap();
            println!("type: {}", au_type);
            while r > 0 {
                let field_name_ptr = auparse_get_field_name(au);
                let field_name = CStr::from_ptr(field_name_ptr as *const _).to_str().unwrap();
                println!("field_name: {}", field_name);

                r = auparse_next_field(au);
            }
            r = auparse_next_record(au);
            println!("r: {}", r);
        }
    };
}
